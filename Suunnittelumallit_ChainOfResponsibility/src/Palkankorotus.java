
public class Palkankorotus {
	
	private double vanhapalkka;
	private double uusipalkka;
	
	public Palkankorotus(double vanhapalkka, double uusipalkka) {
		this.vanhapalkka = vanhapalkka;
		this.uusipalkka = uusipalkka;
	}
	
	public double getUusipalkka() {
		return uusipalkka;
	}
	
	public double getVanhapalkka() {
		return vanhapalkka;
	}
	
	public void setUusipalkka(double uusipalkka) {
		this.uusipalkka = uusipalkka;
	}
	
	public void setVanhapalkka(double vanhapalkka) {
		this.vanhapalkka = vanhapalkka;
	}

}
