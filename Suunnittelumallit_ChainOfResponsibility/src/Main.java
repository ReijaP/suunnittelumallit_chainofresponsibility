import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main {

	public static void main(String[] args) {
		
		Tyontekija työntekijä = new Tyontekija();
		Esimies esimies = new Esimies(2);
		Paallikko päällikkö = new Paallikko(2,5);
		Toimitusjohtaja toimari = new Toimitusjohtaja(5);
		
		esimies.setSuccessor(päällikkö);
		päällikkö.setSuccessor(toimari);
		
		try {
			while(true) {
				System.out.println("Mikä on nykyinen palkka?");
				double palkka = Double.parseDouble(new BufferedReader(new InputStreamReader(System.in)).readLine());
				työntekijä.setPalkka(palkka);
				System.out.println(työntekijä.getPalkka());
				System.out.println("Mitä haluaisist uudeksi palkaksi?");
				double uusipalkka = Double.parseDouble(new BufferedReader(new InputStreamReader(System.in)).readLine());
				esimies.kasittelePyynto(new Palkankorotus(palkka, uusipalkka));
			}
			} catch (Exception e) {
				System.out.println(e);
				System.exit(1);
			}
		}

	}


