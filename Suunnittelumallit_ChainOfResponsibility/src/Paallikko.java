
public class Paallikko extends Toimistohierarkia {
	
	Tyontekija työntekijä = new Tyontekija();
	private double alaprosentti;
	private double yläprosentti;
	
	public Paallikko(int alaprosentti, int yläprosentti) {
		this.alaprosentti = alaprosentti;
		this.yläprosentti = yläprosentti;
		this.alaprosentti = (this.alaprosentti + 100) / 100;
		this.yläprosentti = (this.yläprosentti + 100) / 100;
	}
	
	public void kasittelePyynto(Palkankorotus pyynto) {
		if ((pyynto.getUusipalkka() > pyynto.getVanhapalkka()*alaprosentti) && (pyynto.getUusipalkka() < pyynto.getVanhapalkka()*yläprosentti)){
			System.out.println("Päällikkö hyväksyy " + pyynto.getUusipalkka() + "€ palkankorotuksen.");
		}else {
			System.out.println(alaprosentti);
			System.out.println(yläprosentti);
			super.kasittelePyynto(pyynto);
		}
	}

}
