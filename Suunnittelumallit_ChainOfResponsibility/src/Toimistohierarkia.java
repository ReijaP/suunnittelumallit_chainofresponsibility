
public abstract class Toimistohierarkia {
	
	
	private Toimistohierarkia myontaja;
	
	/*public void getPalkka() {
		this.palkka = Tyontekija.palkka;
	}*/
	
	public void setSuccessor(Toimistohierarkia myontaja) {
		this.myontaja = myontaja;
	}
	
	public void kasittelePyynto(Palkankorotus pyynto) {
		if (myontaja != null) {
			myontaja.kasittelePyynto(pyynto);
		}
	}

}
