
public class Toimitusjohtaja extends Toimistohierarkia{
	
	Tyontekija työntekijä = new Tyontekija();
	private double alaprosentti;
	
	public Toimitusjohtaja(int alaprosentti) {
		this.alaprosentti = alaprosentti;
		this.alaprosentti = (this.alaprosentti + 100) / 100;
	}
	
	public void kasittelePyynto(Palkankorotus pyynto) {
		if (pyynto.getUusipalkka() > pyynto.getVanhapalkka()*alaprosentti){
			System.out.println("Toimitusjohtaja hyväksyy " + pyynto.getUusipalkka() + "€ palkankorotuksen.");
		}else {
			System.out.println("Palkankorotuspyyntö evätty");
		}
	}

}
