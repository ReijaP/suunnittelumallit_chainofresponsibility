
public class Esimies extends Toimistohierarkia{
	
	Tyontekija työntekijä = new Tyontekija();
	private double prosentti;
	
	public Esimies(int hyvaksymisprosentti) {
		this.prosentti = hyvaksymisprosentti;
		this.prosentti = (prosentti + 100) / 100;
	}
	
	public void kasittelePyynto(Palkankorotus pyynto) {
		if (pyynto.getUusipalkka() <= pyynto.getVanhapalkka()*prosentti) {
			System.out.println("Esimies hyväksyy " + pyynto.getUusipalkka() + "€ palkankorotuksen.");
		}else {
			super.kasittelePyynto(pyynto);
		}
	}

}
